#include <stdio.h>
#include "mymalloc.h"
static char myblock[5000];
int notStarted = 1;
void freeNode(unsigned short index1,unsigned short index2,unsigned short index3)
{
  unsigned short* prev = (unsigned short*)&myblock[index1];
  unsigned short* curr = (unsigned short*)&myblock[index2];
  unsigned short* next = (unsigned short*)&myblock[index3];
  *curr -= 1;
  if(*next%2 == 0 && index3 < 5000){
    *curr += (*next+2);
  }
  if(*prev%2 == 0 && index1 != index2){
    *prev += (*curr+2);
  }
}

void myfree(void* toFree,char* fn,int ln)
{
  if(toFree == NULL){
    printf("Error Cannot Free NULL--File: %s Line: %i\n",fn,ln);
    return;
  }
  unsigned short n = 0;
  unsigned short nPrev = 0;
  while(n < 5000){
    unsigned short* temp = (unsigned short*)&myblock[n];	
    if(temp+1 == (unsigned short*)toFree){
      if(*temp%2!=0){
        freeNode(nPrev,n,n+(*temp&~1)+2);
        return;
      }
      else{
        printf("Error Memory is Already Free--File: %s Line: %i\n",fn,ln);
        return;	
      }
    }
    nPrev = n;
    n += (*temp&~1)+2;
  }
  printf("Error Cannot Find Memory--File: %s Line: %i\n",fn,ln);
  return;
}  

unsigned short* createNode(unsigned short index,size_t size)
{
  unsigned short* temp1 = (unsigned short*)&myblock[index];
  unsigned short distance1 = (unsigned short)size;
  unsigned short distance2 = *temp1-distance1-2;
  *temp1 = distance1;
  unsigned short* temp2 = (unsigned short*)&myblock[index+distance1+2];
  *temp2 = distance2;
  if(*temp2 == 0)*temp1 += 2;
  *temp1 += 1;
  return temp1;
}

void* mymalloc(size_t size,char* fn,int ln)
{
  if(notStarted){
    unsigned short* first = (unsigned short*)myblock;
    *first = 4998;
    notStarted = 0;
  }
  //Memory is only allocated in multiples of 4
  int k = size%4;
  size+=(4-k);
  //Obviously if they ask for more memory than we could possibly have, thery're not going to get a good result.
  if(size > 4998){
  	return NULL;
  }
  //This loop functions by traversing our implicit list. The first node will always be at index 0, so obviously we start there.
  unsigned short n = 0;
  while(n < 5000){
    unsigned short* temp = (unsigned short*)&myblock[n];	
    //If the size value of the metadata is even, we know the space has not been allocated, if there is enough space and it is unallocated, we can create our node
    if(*temp%2==0 && *temp >= (size+2)){
      //Create the node, return the void* to your creation
      return (void*)(createNode(n,size)+1);
    }
    n += (*temp&~1)+2;
  }
  return NULL;
}

/**
Remember to ask Tjang why the puts statements were printing whenever you ran the program
*/