#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>



typedef struct node
{
	struct node* next;
	uint32_t tag;
	uint32_t validBit;
	uint32_t address;
	int dirtyBit;
}block;

//Input should be of the form
//./c-sim <cache size> <associativity> <block size> <replacement_policy> <write_policy> <trace file> 
int cacheSize,setSize,blockSize,replacementPolicy,writePolicy,blockOffsetBits,indexBits,tagBits,cacheHits,cacheMisses,memoryWrites,memoryReads; //These globals are used so that all the information gleaned from the inputs is readily avaialable for all methods

void runCache(char* name)
{
	//This block of code creates the cache to the user's specifications, it first mallocs an array of block* in the heap, then goes through and puts a linked list in each index.
	block** cache = (block**)malloc( sizeof(block*)*(cacheSize/(setSize*blockSize)) );
	int i = 0;
	int k;
	for(;i < (cacheSize/(setSize*blockSize));i++){//Iterates through the sets
		k = 0;
		for(;k < setSize;k++){//Creates the blocks contained within the sets
			block* temp = (block*)malloc( sizeof(block) );
			temp->address = 0;
			temp->tag = 0;
			temp->validBit = 0;
			temp->dirtyBit = 0;
			temp->next = NULL;
			if(k == 0){
				cache[i] = temp;

			}
			else{
				temp->next = cache[i]->next;
				cache[i]->next = temp;
			}
		}
	}

	//This block of code opens the file, sets up for scanning in
	FILE *filename;
	char unimportantMemoryAddress[10];
	char readOrWrite[3];
	char memoryAddress[10];
	char* ptr;
	filename = fopen(name,"r");
	while(!feof(filename)){
		fscanf(filename,"%s %s %s",unimportantMemoryAddress,readOrWrite,memoryAddress);
		if(unimportantMemoryAddress[0] == '#')break;//When we reach the end of the file, we'll scan in "#eof", this checks for that happening and breaks out if it does
		uint32_t address = (uint32_t)strtol(memoryAddress,NULL,0);
		uint32_t tag = (address>>(blockOffsetBits+indexBits))&((1<<tagBits)-1);
		uint32_t index = (address>>blockOffsetBits)&((1<<indexBits)-1);
		if(tagBits == 32)tag = address;
		if(blockOffsetBits == 0)index = address&((1<<indexBits)-1);
		//The line below is for debugging purposes, I'm commenting it out, but leaving it in to facilitate testing if any errors occur
		//printf("%s %s %s Address:%i Tag:%i Index:%i memoryWrites:%i ",unimportantMemoryAddress,readOrWrite,memoryAddress,address,tag,index,memoryWrites);
		block* temp = cache[index];
		block* prev = NULL;
		//This while loop is the code for lookups, essentially it starts at the appropriate cache index, then does a linear search through through the linked list
		while(temp != NULL){
			if(temp->validBit == 1 && temp->tag == tag){//If this statement succeeds it means you got a hit
				cacheHits++;
				temp->validBit = 1;
				if(readOrWrite[0] == 'W'){//If it's a write
					if(writePolicy == 0){//write through
						memoryWrites++;
					}
					else{//write back
						temp->dirtyBit = 1;
					}
				}
				if(replacementPolicy == 1 && prev != NULL){//If it's LRU
					prev->next = temp->next;
					temp->next = cache[index];
					cache[index] = temp;
				}
				break;
			}
			prev = temp;
			temp = temp->next;
		}
		//This if statement is only triggered if you got a miss, that's because from the while loop above, temp will only be NULL if you got through the whole linked list without getting a hit
		if(temp == NULL){
			cacheMisses++;
			memoryReads++;
			if(writePolicy == 0 && readOrWrite[0] == 'W')memoryWrites++;//If it's write through you can just go ahead in increment memory writes
			if(replacementPolicy == 0){//FIFO
				temp = cache[index];
				prev = NULL;
				//This is the protocol for a cold miss in FIFO, it runs a search on the blocks for an empty block and writes in if it can
				while(temp != NULL){
					if(temp->validBit == 0){
						temp->address = address;
						temp->validBit = 1;
						temp->tag = tag;
						if(writePolicy == 1 && readOrWrite[0] == 'W')temp->dirtyBit = 1;
						break;
					}
					prev = temp;
					temp = temp->next;
				}
				//If you have a capacity miss, this code block enacts FIFO replacement procedure, evicting and freeing the first node after appending the new node to the end of the list
				if(temp == NULL){
					prev->next = (block*)malloc( sizeof(block) );
					prev->next->validBit = 1;
					prev->next->tag = tag;
					prev->next->address = address;
					prev->next->dirtyBit = 0;
					if(readOrWrite[0] == 'W' && writePolicy == 1)prev->next->dirtyBit = 1;//Checks to see if the data you're evicting is dirty
					block* temp1 = cache[index];
					cache[index] = cache[index]->next;
					if(temp1->dirtyBit == 1)memoryWrites++;
					free(temp1);
				}
			}
			else{//LRU, this code is a little bit simpler than the FIFO one because the program behaves the same for a cold miss and a capcity miss, it adds the new node to the start
				 //of this list, flips the dirty bit, and removes the last node of the list.
				temp = cache[index];
				cache[index] = (block*)malloc( sizeof(block) );
				cache[index]->next = temp;
				cache[index]->tag = tag;
				cache[index]->address = address;
				cache[index]->validBit = 1;
				cache[index]->dirtyBit = 0;
				if(writePolicy == 1 && readOrWrite[0] == 'W')cache[index]->dirtyBit = 1;//Checks to see if the data you're evicting is dirty
				temp = cache[index];
				prev = cache[index];
				while(temp->next != NULL){
					prev = temp;
					temp = temp->next;
				}
				prev->next = NULL;
				if(temp->dirtyBit == 1)memoryWrites++;
				free(temp);
			}
		}
	}
	//Frees everything
	for(i = 0;i < (cacheSize/(setSize*blockSize));i++){
		block* temp = cache[i];
		block* prev;
		while(temp != NULL){
			prev = temp;
			temp = temp->next;
			free(prev);
		}
	}
	free(cache);
	//Prints the output
	printf("Memory reads: %i\nMemory writes: %i\nCache hits: %i\nCache misses: %i\n",memoryReads,memoryWrites,cacheHits,cacheMisses);
}

//Main serves entirely to parse the input and make sure that everything is sanitary and pretty
int main(int argc,char **argv)
{
	//Verify that number of arguments is correct
	if(argc != 7){
		puts("Invalid number of arguments");
		return 1;
	}
	//Convert cache size into int and ensure that the input was appropriate
	cacheSize = atoi(argv[1]);
	if(cacheSize <= 0 || (log2((double)cacheSize)-(int)log2((double)cacheSize)) != 0){
		puts("Invalid cache size");
		return 1;
	}
	//Determine block size
	blockSize = atoi(argv[3]);
	if(blockSize <= 0 || cacheSize%blockSize != 0 || (log2((double)blockSize)-(int)log2((double)blockSize)) != 0){
		puts("Invalid block size");
		return 1;
	}
	//Determine associativity
	if(!strcmp(argv[2],"direct")){//Direct mapped
		setSize = 1;
	}
	else if(!strcmp(argv[2],"assoc")){//Fully associative
		setSize = cacheSize/blockSize;
	}
	else if(strlen(argv[2]) > 6 && argv[2][0] == 'a' && argv[2][1] == 's' && argv[2][2] == 's' && argv[2][3] == 'o' && argv[2][4] == 'c' && argv[2][5] == ':'){//n-way associative
		setSize = atoi(argv[2]+6);
		if(setSize <= 0 || (log2((double)setSize)-(int)log2((double)setSize)) != 0){
			puts("Invalid associativity");
			return 1;
		}
	}
	else{//Bad associativity
		puts("Invalid associativity");
		return 1;
	}
	//Determine replacement policy
	if(!strcmp(argv[4],"FIFO"))replacementPolicy = 0; //Replacement policy of 0 denotes that the replacement policy is FIFO
	else if(!strcmp(argv[4],"LRU"))replacementPolicy = 1; //Replacement policy of 1 denotes that the replacement policy is LRU
	else{
		puts("Invalid replacement policy");
		return 1;
	}
	//Determine write policy
	if(!strcmp(argv[5],"wt"))writePolicy = 0; //writePolicy of 0 denotes that the write policy is write through
	else if(!strcmp(argv[5],"wb"))writePolicy = 1; //writePolicy of 1 denotes that the write policy is write back
	else{
		puts("Invalid write policy");
		return 1;
	}
	//If your program doesn't satisfy <cacheSize> = #sets * <setSize> * <blockSize> then you're gonna get an error
	if((setSize*blockSize) > cacheSize){
		puts("Invalid input");
		return 1;
	}
	//This is debugging code, just commented it out but left it in to make bug checking easier, if it should come to that
	//printf("%i %i %i %i %i %s\n",cacheSize,setSize,blockSize,replacementPolicy,writePolicy,argv[6]);
	//Make sure the file name is real
	FILE *filename;
	filename = fopen(argv[6],"r");
	if(!filename){
		puts("Invalid filename");
		return 1;
	}
	fclose(filename);

	blockOffsetBits = (int)log2((double)blockSize);
	indexBits = (int)log2((double)(cacheSize/(setSize*blockSize)));
	tagBits = 32-(blockOffsetBits+indexBits);
	//
	cacheHits = 0;
	cacheMisses = 0;
	memoryWrites = 0;
	memoryReads = 0;
	runCache(argv[6]);

}