#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/**
NOTE:
This file uses strcasecmp to compare words alphabetically. As such, case is not considered while sorting, 'A' is equivalent to 'a'. Because of this it is an unstable algorithm.
While it is guaranteed to print a series of words in alphabetical order, the order in which equivalent words with differing cases appear in the output will be the opposite of the 
order they appeared in in the input string(i.e. if the input string were "an,An,aN,AN,BE,Be,bE,be" the output would be in the order AN->aN->An->an->be->bE->Be->BE).
*/

/**
This is a simple struct which we will use as the nodes of our linked list.
*/
typedef struct node
{
	char *data;
	struct node *next;
}node_s;

/**
This is a global array which we will use to hold linked lists. The 26 indices of buckets correspond to the 26 letters of the alphabet, with 'A' being 0, 'B' being 1 etc. 
buckets[0] should contain a sorted linked list of all words in the input string beginning with 'a' or 'A', while buckets[1] should contain a sorted linked list of all words 
in the input string beginning with 'b' or 'B', and so on. 

The purpose of buckets is to reduce the total number of comparisons required to sort the list. In this case, insertion sort would mean approximately k^2 operations to sort a 
list of k words. By breaking the list up into different buckets, it means we're performing a number of smaller insertion sorts.
*/
node_s* buckets[26] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

/**
The method fillBuckets takes in the first character of a word, and the word's length. It proceeds to create a node, copy that word into the data portion of the created node, and
insert that node into the proper index of buckets. 

First, index is calculated by casting beginning to int and subtracting 65 if it was a capital letter, 97 if it was lower case (this takes advantage of ASCII value, as 'A'-'Z' have 
the values 97-122 and 'a'-'z' have the values 65-90). Next, the method performs a simple insertion into the appropriate place (by ascending alphabetical order) in buckets[index].

Once all of the words in the input string have been inserted into buckets using fillBuckets, buckets[0] should contain a sorted list of all words in the string beginning with 'a'
or 'A', buckets[1] should contain a sorted list of all words in the string beginning with 'b' or 'B', and so on.
*/
void fillBuckets(char *beginning,size_t len)
{
	int index = (int) *beginning;
	if(index > 96)index -= 97;
	else index -= 65;
	node_s *target = (node_s*)malloc( sizeof(node_s) );
	target->next = NULL;
	target->data = (char*)malloc(len);
	strncpy(target->data,beginning,len);
	target->data[len] = '\0';
	//If buckets[index] is empty, then buckets[index] is simply pointed to target
	if(buckets[index] == NULL){
		buckets[index] = target;
		return;
	}
	//If the word in target alphabetically precedes the first word in the linked list, then target becomes the new head of the linked list contained at buckets[index]
	if(strcasecmp(buckets[index]->data,target->data)>=0){
		target->next = buckets[index];
		buckets[index] = target;
		return;
	}
	//temp iterates through the linked list contained in buckets[index], when it finds a word that comes after it alphabetically, it inserts itself into the linked list after the aforementioned node
	node_s *temp = buckets[index];
	while(temp->next != NULL){
		if(strcasecmp(temp->next->data,target->data)>=0){
			target->next = temp->next;
			temp->next = target;
			return;
		}
		temp = temp->next;
	}
	//If there are no nodes that come after target alphabetically, target is added to the end of the list
	temp->next = target;
	target->next = NULL;
	return;
}
/**
This method iterates through buckets, a global array of size 26 which holds nodes, each time it finds a linked list contained in buckets, it uses a while loop to 
iterate through the list and prints out the string held in each node.
*/
void printBuckets()
{
	node_s* temp;
	int i;
	for(i = 0;i < 26;i++){
		if(buckets[i] != NULL){
			temp = buckets[i];
			while(temp != NULL){
				printf("%s\n",temp->data);
				temp = temp->next;
			}
		}
	}
}
/**
The method tokenizer finds the words in the input string and passes them into the fillBuckets method.

The method itself places a pointer at the start of the input string, then iterates through the input. It uses the variable len to track the length of words within the string.
Each time the loop iterates it first increments len, next, it checks if the current character is alphabetic. If the character is alphabetic, then it simply increments s and
moves on to the next character.
If the current character is not alphabetic, then the program checks to make sure that len is not 1, if len is 1, then we know that the current non-alphabetic character is not
directly preceded by a word (either there are multiple separators, or there is a separator at the start of the string). If there is no word to pass to fillBuckets, then 
fillBuckets is not called.
If len != 0, then the fillBuckets is called and passed the char pointer k, which is a reference to the first character of the word preceding the curret non-alphabetic character,
and size_t len, the length of the word.  
Whether or not len equals 1, each time the method comes across a non-alphabetic character it points k to the character following the current character, and resets len to 0.

Once the loop completes, there is an if-statement which inserts the final word of the string. In the previous loop, fillBuckets was only called when we reached a non-alphabetic 
character, since the string should not end with a non-alphabetic character, this line of code calls fillBuckets on the final word. Note that if the string does end with
a non-alphabetic character, the if condition makes sure that fillBuckets is not called again. Each time the loop encounters a non-alphabetic character it resets len to 0, 
the if statement checks to make sure len is not 0 before calling fillBuckets.
*/

void tokenizer(char *word)
{
	char *s = word;
	char *t = s+strlen(s);
	size_t len = 0;
	char *k = s;
	while(s < t){
		len++;
		if(!isalpha(*s)){
			if(len != 1)fillBuckets(k,len-1);
			len = 0;
			k = s+1;
		}
		s = s+1;
	}
	if(len != 0)fillBuckets(k,len+1);
	printBuckets();
}

/**
The main method checks to make sure that the user has provided the correct number of arguments, failing to do so will result in either an error message stating that too few arguments
have been provided, or an error message sayhing that too many have been provided.

If the user has provided the correct number of inputs then the program calls tokenizer.
*/
int main(int argc,char **word)
{
	if(argc < 2){
		puts("Error: Too few arguments");
		return 1;
	}
	else if(argc > 2){
		puts("Error: Too many arguments");
		return 1;
	}
	tokenizer(word[1]);
	return 0;
}