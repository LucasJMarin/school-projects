#ifndef MYMALLOC_H
#define MYMALLOC_H
void freeNode(unsigned short index1,unsigned short index2,unsigned short index3);
#define free(x) myfree(x,__FILE__,__LINE__);
unsigned short* createNode(unsigned short index,size_t size);
#define malloc(x) mymalloc(x,__FILE__,__LINE__);
#endif