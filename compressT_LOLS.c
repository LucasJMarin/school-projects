#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>

//This struct is made to hold all the data the LOLS() method needs to effectively compress a file.
typedef struct node
{
	char* name_to_open; 	//Name of the input file
	char* name_to_write; 	//Name of the file the thread will output to
	int start; 				//Offset from the start of the file i.e. if start = 3, reading will start at the third character of the input file	
	int run;				//Number of characters to be read in
}filename_startpoint_size;


void LOLS(filename_startpoint_size *forCompression)
{

	//Opens the files to read from and the file to write to
	FILE *filename_read;
	FILE *filename_write;
	filename_read = fopen(forCompression->name_to_open,"r");
	filename_write = fopen(forCompression->name_to_write,"w");

	//Sets up the variables for the for loop
	int i = 0;
	char a = 'a';
	char b = '\0';
	int currentTotal = 0;

	//Guarantees that I start from the appropriate part of the file
	fseek(filename_read,forCompression->start,SEEK_SET);
	for(;i<=forCompression->run;i++){
		fscanf(filename_read,"%c",&a); //The file is read in character by character
		if( !isalpha(a) ){ //This if is triggered if the character read in is not alphabetic, it also checks to make sure that the character was not the last one in the segment to be read
			if(i == forCompression->run){
				if(currentTotal == 1) { //If the non-alphabetic character is the last character in the section to be read, it prints out whatever it had read in before that character
					fprintf(filename_write,"%c",b);
				}
				else if(currentTotal == 2){ 
					fprintf(filename_write,"%c%c",b,b);
				}
				else{
			 		 if(currentTotal != 0)fprintf(filename_write,"%i%c",currentTotal,b); 
				}
			}
			continue;
		}
		//This if statements verifies that (the current character equals the previous character || you're at the first character) && you are not at the last character   
		if((a == b || b == '\0') && i != forCompression->run)currentTotal++; 
		//In the event that the previous check fails, the program prints the compressed version of the collection of letters it was on
		else{
			if(currentTotal == 1) {
				fprintf(filename_write,"%c",b);
			}
			else if(currentTotal == 2){ 
				fprintf(filename_write,"%c%c",b,b);
			}
			else{
			    fprintf(filename_write,"%i%c",currentTotal,b);
			}
			currentTotal = 1;
		}
		b = a;
	}

	//The filename_startpoint_size struct must be freed here in order to prevent a memory leak
	free(forCompression->name_to_open);
	free(forCompression->name_to_write);
	free(forCompression);
}

int main(int argc,char **name)
{
	//The following code block checks that the inputs are valid and correctly formatted, and prepares the fields I'll need for the compression
	if(argc != 3){ 		//Checks to make sure the user has put in the appropriate number of fields
		puts("Error: Improper number of arguments");
		return 1; 
	}
	FILE *filename;
	filename = fopen(name[1],"r");
	if(!filename){		//This assures that the input name provided can be resolved as a file name
		printf("Error: '%s' cannot be found\n",name[1]);
		return 1;
	}
	int file_size;
	int num_multifiles = atoi(name[2]); 
	fseek(filename, 0L, SEEK_END);
	file_size = ftell(filename);
	fclose(filename);
	if(num_multifiles <= 0 || num_multifiles > file_size){	//This ensures that the number of multifiles makes sense for the input
		puts("Error: Number of multifiles cannot be less than 1 or greater than the number of characters in the file");
	}
	pthread_t threads[num_multifiles];  //This array is going to hold the ID's of every thread, so that they can all be joined at the end of the program.

	//This code block reformats the name of the input file to get the name of the output file(s), a.txt gives a_txt_LOLS, not that it also checks to see if the file has a .txt suffix, and edits the output filename accordingly
	char name_to_write[strlen(name[1])+16];
	if(strlen(name[1]) < 5 || *(name[1]+strlen(name[1])-4) != '.'){
		strcpy(name_to_write,name[1]);
		strcat(name_to_write,"_LOLS");
	}
	else{
		name_to_write[strlen(name[1])+15] = '\0';
		strcpy(name_to_write,name[1]);
		*(name_to_write+strlen(name[1])) = 'a';
		strcpy(name_to_write+strlen(name[1])-4,"_txt_LOLS");
	}
	//This code block creates the first filename_startpoint_size object and launches the first thread
	filename_startpoint_size *forCompression = (filename_startpoint_size*)malloc( sizeof(filename_startpoint_size) );
	forCompression->name_to_open = malloc( strlen(name[1]) );
	strcpy(forCompression->name_to_open,name[1]);
	forCompression->name_to_write = malloc( strlen(name[1]) + 20);
	strcpy(forCompression->name_to_write,name_to_write);
	if(num_multifiles != 1){	//This check sees whether the user wants the program to create more than one output file and changes the name of the output file accordingly
		char pnum[33];
		snprintf(pnum, 33, "%d", 0);
		strcat(forCompression->name_to_write,pnum);
	}
	forCompression->start = 0;
	forCompression->run = (file_size/num_multifiles)+(file_size%num_multifiles); 
	int startpt = forCompression->run; //startpt is used by the following threads to know what character they should start reading the input file from
	pthread_create(&threads[0],NULL,&LOLS,forCompression); //This creates the first thread and saves its ID to the threads array created before

	//This loop serves to set up and activate all the threads following the first, the setup is largely the same
	int i = 1;
	for(;i<num_multifiles;i++){
		filename_startpoint_size *forCompression = (filename_startpoint_size*)malloc( sizeof(filename_startpoint_size) );
		forCompression->name_to_open = malloc( strlen(name[1]) );
		strcpy(forCompression->name_to_open,name[1]);
		forCompression->name_to_write = malloc( strlen(name[1]) + 10);
		strcpy(forCompression->name_to_write,name_to_write);
		char pnum[33];
		snprintf(pnum, 33, "%d", i);
		strcat(forCompression->name_to_write,pnum);
		forCompression->start = startpt;
		forCompression->run = (file_size/num_multifiles);
		startpt += forCompression->run;
		//Create the thread, pass in a as a parameter
		pthread_create(&threads[i],NULL,&LOLS,forCompression);
	}
	for(i = 0;i < num_multifiles;i++)pthread_join(threads[i], NULL); //This loop joins all of the previously created threads
	return 0;
}