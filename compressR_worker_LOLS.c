#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int LOLS(char *name, char* pnumString, int size, int numfiles){

	int pnum = atoi(pnumString);

	
	//Open the file
	FILE *filename;
	filename = fopen(name,"r");
	//Make sure the file exists
	if(!filename){
		puts("Error");
		return 0;
	}

	int pnumint = atoi(pnumString);

	//Determines number of characters to read
	//Standard case
	int max = (size/numfiles);
	if(pnumint == 0){
		//Special case, takes care of remainder
		max += size%numfiles;
	}else{
		//Sets up the file to read from the right character
		long int offset = pnumint*(size/numfiles) + (size%numfiles);
		fseek(filename, offset, SEEK_SET);
	}

	//The previous character
	char char1 = 'a';
	//The current character
	char char2 = '\0';
	int currentTotal = 0;

	//Create the new file with the right name*********************
	int fsize = strlen(name);		//Find size of file name
	int pnumsize = strlen(pnumString);	//Find the size of the process number
	char newfname[fsize+pnumsize+12];	//New string
	if(strlen(name) < 5 || name[fsize-4] != '.'){
		strcpy(newfname, name);			//Adds filename
		strcat(newfname, "_LOLS");		//Adds "_LOLS"
		strcat(newfname, pnumString);	
	}
	else{
	name[fsize-4] = '\0';
	strcpy(newfname, name);			//Adds filename
	strcat(newfname, "_txt_LOLS");		//Adds "_txt_LOLS"
	strcat(newfname, pnumString);		//Adds process number
	//strcat(newfname, ".txt");		//Adds ".txt"
	}
	FILE* newfile = fopen(newfname, "w");
	int i;
	for(i = 0; i <= max; i++){

		fscanf(filename,"%c",&char1);
		if( !isalpha(char1) ){ //This check sees whether or not the character read in is alphabetic, it also checks to make sure that it isn't the last character in the segment to be read
			if(i == max){ //If it is the last character, then the program prints out whatever characters it had read in before that non-alphabetic character
				if(currentTotal == 1){
					fprintf(newfile,"%c", char2);
				}
				else if(currentTotal == 2){
					fprintf(newfile,"%c%c", char2, char2);
				}
				else{
					if(currentTotal != 0)fprintf(newfile,"%i%c", currentTotal, char2);
				}				
			}
			continue;
		}
		//If 'a' is a repeat or the first of a new chain
		if((char1 == char2 || char2 == '\0') && i!= max){

			currentTotal++;

		}else{

			if(currentTotal == 1){
				fprintf(newfile,"%c", char2);
			}
			else if(currentTotal == 2){
				fprintf(newfile,"%c%c", char2, char2);
				
			}
			else{
				fprintf(newfile,"%i%c", currentTotal, char2);
			}

			currentTotal = 1;

		}
		char2 = char1;
	}
	return 1;
}

int main(int argc, char **name){

	//File size
	int size = atoi(name[3]);
	//Number of files to be made
	int numfiles = atoi(name[4]);
	
	LOLS(name[1], name[2], size, numfiles);
	
}
