#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]){

	if(argc!=3){
		puts("Error: Improper number of arguments");
		return 1;
	}

	char* input = argv[1];
	char* mode = "r";
	FILE* fp = fopen(input,mode);
	if(!fp){
		printf("Error: Cannot resolve to a filename");
		return 1;
	}

	//size represents the number of characters in the file
	fseek(fp, 0L, SEEK_END);
	int size = ftell(fp);
	char sizeString[33];
	snprintf(sizeString, 33, "%d", size);
	fclose(fp);


	//Inputs will be
	// 1: file name
	// 2: number of files to be split into

		
	//Each child process should open the file on its own, so fork before fopen
	
	pid_t PID;

	//Number of files to be made
	int numfiles = atoi(argv[2]);
	int i;
	for(i = 0; i<numfiles; i++){

		if((PID = fork()) == -1){

			printf("Fork failed for process %d\n", i);

		}
		if(PID == 0){

			//pnum needs to be a null terminated character string
			char pnum[33];
			snprintf(pnum, 33, "%d", i);
			if(numfiles == 1)pnum[strlen(pnum)-1] = '\0';
			//A char array with the file name, process number, # of chars in file, and # of files to be made
			char *const parameters[] = {"./worker", argv[1], pnum, sizeString, argv[2], NULL};
			execv("./worker", parameters);
			printf("Exec failed for process %d\n", i);
			break;

		}
	}

	for(i = 0; i<numfiles; i++){
		waitpid(-1,NULL,WUNTRACED);
	}

}
